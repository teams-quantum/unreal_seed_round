//SPDX-License-Identifier: UNLICENSED
pragma solidity 0.8.7;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";

contract UnReal is
    Initializable,
    UUPSUpgradeable,
    OwnableUpgradeable,
    PausableUpgradeable,
    ReentrancyGuardUpgradeable
{
    string public constant name = "LaunchPad";
    using SafeERC20Upgradeable for IERC20Upgradeable;

    //  total amount staked on pool
    mapping(uint256 => uint256) public totalContributedAmountInPool;
    // balance stakable on pool
    mapping(uint256 => uint256) private balanceStakableAmount;
    mapping(address => bool) public blacklisted;
    mapping(address => bool) public authorizedUser;
    // Info of each pool.
    PoolInfo[] public poolInfo;

    // Info of each user that stakes LP tokens.
    mapping(uint256 => mapping(address => UserInfo)) public userInfo;
    uint256 private poolIds;
    // Info of each user.
    struct UserInfo {
        uint256 amount; // How many LP tokens the user has provided.
        uint256 stakingStartTime; // Staking start time in pool
        uint256 stakingEndTime; // Staking End time in pool
        bool hasStaked; // check is account staked
        bool isStaking; // check is account currently staking
        //@dev enable if needed
        // uint256 depositReward; // Reward for deposit
        // uint256 interestReward; // Interest for staking period
        uint256 expectedReward; // expected reward of staking
    }

    // Info of each pool.
    struct PoolInfo {
        IERC20Upgradeable lpToken; // Address of LP token contract.
        string name;
        string symbol;
       
        uint256 startTime; // Lp's start time
        uint256 endTime; // Lp's end time
        /** 
        @dev enable if reward needed
        IERC20Upgradeable rewardToken; // Address of Reward token contract
        uint256 rewardRate; // reward rate in percentage (APR)
        uint256 duration; // duration of each user staking time to collect reward
        uint256 depositRewardRate; // user will get deposit-reward (stake amount * % / 100)
        */
        uint256 poolStakableAmount; // pools total stakable amount
        uint256 minimumContributeAmount;
        uint256 id;
    }

    event Reward(address indexed from, address indexed to, uint256 amount);
    event StakedToken(address indexed from, address indexed to, uint256 amount);
    event UpdatedStakingEndTime(uint256 endTime);
    event WithdrawAll(
        address indexed user,
        uint256 indexed pid,
        uint256 amount
    );
    event AccountblacklistUpdated(address indexed account, bool status);
    event AccountsblacklistUpdated(address[] indexed accounts, bool status);
    event AccountsauthorizedUserUpdated(
        address[] indexed accounts,
        bool status
    );
    event AirDropStatus(address[] indexed _recipients, uint256[] _amount);
    event TokenFromContractTransferred(
        address externalAddress,
        address toAddress,
        uint256 amount
    );
    event WithdrawBNBFromContract(address indexed to, uint256 amount);

    /// @custom:oz-upgrades-unsafe-allow constructor
    constructor() initializer {}

    //to recieve BNB from uniswapV2Router when swaping
    receive() external payable {}

    function initialize() public initializer {
        // initializing
        __Ownable_init_unchained();
        __Pausable_init_unchained();
        __ReentrancyGuard_init_unchained();
    }

    function _authorizeUpgrade(address) internal override onlyOwner {}

    /**
       @dev get pool length
       @return current pool length
    */
    function poolLength() external view returns (uint256) {
        return poolInfo.length;
    }

    /**
       @dev get current block timestamp
       @return current block timestamp
    */
    function getCurrentBlockTimestamp() external view returns (uint256) {
        return block.timestamp;
    }

    /**
       @dev setting staking pool end time
       @param _pid index of the array i.e pool id
       @param _endTime when staking pool ends
    */
    function setPoolStakingEndTime(uint256 _pid, uint256 _endTime)
        external
        virtual
        onlyOwner
        whenNotPaused
    {
        require(
            poolInfo[_pid].endTime > poolInfo[_pid].startTime,
            "end time must be greater than start time"
        );
        poolInfo[_pid].endTime = _endTime;
        emit UpdatedStakingEndTime(_endTime);
    }

    /** 
       @dev returns the total staked tokens in pool and it is independent of the total tokens in pool keeps
       @param _pid index of the array i.e pool id
       @return total staked amount in pool
    */
    function getTotalStakedInPool(uint256 _pid)
        external
        view
        returns (uint256)
    {
        return totalContributedAmountInPool[_pid];
    }

    /** 
       @dev returns the total staked user tokens in pool and it is independent of the total tokens in pool keeps
       @param _pid index of the array i.e pool id
       @return user staked balance in particular pool
    */
    function getUserStakedTokenInPool(uint256 _pid, address _address)
        external
        view
        returns (uint256)
    {
        return userInfo[_pid][_address].amount;
    }

    /**
       @dev Add a new lp to the pool. Can only be called by the owner.
       @param _lpToken user staking token
      
       @param _startTime when pool starts
       @param _endTime when pool ends
   
    */
    function addPool(
        IERC20Upgradeable _lpToken,
        string memory _name,
        string memory _symbol,
        uint256 _startTime,
        uint256 _endTime,
        uint256 _minimumcontributeAmount,
        uint256 _poolStakableAmount
    ) public {
        require(authorizedUser[msg.sender], "You are not an authorizedUser");
        uint256 poolId = poolIds + 1;
        poolIds += 1;
        // _beforeAddPool(_startTime, _endTime, _rewardRate);
        poolInfo.push(
            PoolInfo({
                lpToken: _lpToken,
                name: _name,
                symbol: _symbol,
                startTime: _startTime,
                endTime: _endTime,
                minimumContributeAmount: _minimumcontributeAmount,
                poolStakableAmount: _poolStakableAmount,
                id: poolId
            })
        );
    }

    /**
       @dev AddPool validations.
       @param _startTime when pool starts
       @param _endTime when pool ends
      
    */
    function _beforeAddPool(uint256 _startTime, uint256 _endTime)
        internal
        virtual
    {
        require(
            block.timestamp >= _startTime,
            "STAKING: Start Block has not reached"
        );
        require(block.timestamp <= _endTime, "STACKING: Has Ended");
    }

    /**
       @dev Stake LP token's.
       @param _pid index of the array i.e pool id
       @param _amount staking amount
    */
    function stakeTokens(uint256 _pid, uint256 _amount)
        external
        virtual
        whenNotPaused
    {
        _beforeStakeTokens(_pid, _amount);
        require(!blacklisted[msg.sender], "Swap: Account is blacklisted");
        UserInfo storage user = userInfo[_pid][msg.sender];
        bool transferStatus = poolInfo[_pid].lpToken.transferFrom(
            msg.sender,
            address(this),
            _amount
        );
        if (transferStatus) {
            // update user staking balance in particular pool
            user.amount = user.amount + _amount;
            // update Contract Staking balance in pool
            balanceStakableAmount[_pid] =
                poolInfo[_pid].poolStakableAmount -
                totalContributedAmountInPool[_pid];
            require(block.timestamp < poolInfo[_pid].endTime, "Staking ended");
            require(
                (totalContributedAmountInPool[_pid] + _amount) <=
                    poolInfo[_pid].poolStakableAmount,
                "Total Amount must be less than stakable amount"
            );
            // save the time when they started staking in particular pool
            user.stakingStartTime = block.timestamp;
            totalContributedAmountInPool[_pid] += _amount;

            // update staking status in particular pool
            user.hasStaked = true;
            user.isStaking = true;
            emit StakedToken(msg.sender, address(this), _amount);
        }
    }

    function _beforeStakeTokens(uint256 _pid, uint256 _amount)
        internal
        virtual
    {
        require(_amount > 0, "STAKING: Amount cannot be 0");
        require(_pid <= poolInfo.length, "Withdraw: Pool not exist");
        require(
            poolInfo[_pid].lpToken.balanceOf(msg.sender) >= _amount,
            "STAKING: Insufficient stake token balance"
        );
    }

    /**
     @dev Include multiple address for blacklisting
     @param accounts - blacklisting addresses
    */
    function includeAllInblacklist(address[] memory accounts)
        external
        onlyOwner
    {
        for (uint256 account = 0; account < accounts.length; account++) {
            if (!blacklisted[accounts[account]]) {
                blacklisted[accounts[account]] = true;
            }
        }
        emit AccountsblacklistUpdated(accounts, true);
    }

    /**
     @dev Exclude multiple address from blacklisting
     @param accounts - blacklisting address
     */
    function excludeAllFromblacklist(address[] memory accounts)
        external
        onlyOwner
    {
        for (uint256 account = 0; account < accounts.length; account++) {
            if (blacklisted[accounts[account]]) {
                blacklisted[accounts[account]] = false;
            }
        }
        emit AccountsblacklistUpdated(accounts, false);
    }

    /**
    @param _pid index of the pool
     */
    function balanceStakableToken(uint256 _pid) public view returns (uint256) {
        return (poolInfo[_pid].poolStakableAmount -
            totalContributedAmountInPool[_pid]);
    }

    /** 
    @dev Pause contract by owner
    */
    function pauseContract() external virtual onlyOwner {
        _pause();
    }

    /**
    @dev Unpause contract by owner
    */
    function unPauseContract() external virtual onlyOwner {
        _unpause();
    }

    /**
     @dev Include multiple address for authorizedUsering
     @param accounts - authorizedUsering addresses
    */
    function includeAllInauthorizedUser(address[] memory accounts)
        external
        onlyOwner
    {
        for (uint256 account = 0; account < accounts.length; account++) {
            if (!authorizedUser[accounts[account]]) {
                authorizedUser[accounts[account]] = true;
            }
        }
        emit AccountsauthorizedUserUpdated(accounts, true);
    }

    /**
     @dev Exclude multiple address from authorizedUsering
     @param accounts - authorizedUsering address
     */
    function excludeAllFromauthorizedUser(address[] memory accounts)
        external
        onlyOwner
    {
        for (uint256 account = 0; account < accounts.length; account++) {
            if (authorizedUser[accounts[account]]) {
                authorizedUser[accounts[account]] = false;
            }
        }
        emit AccountsauthorizedUserUpdated(accounts, false);
    }

    /**
    @param _recipients addresses to receive tokens/airdrop
    @param _amount amount to be send.
     */
    function AirdropdropTokens(
        IERC20Upgradeable rewardToken,
        address[] memory _recipients,
        uint256[] memory _amount
    ) public onlyOwner returns (bool) {
        for (uint256 i = 0; i < _recipients.length; i++) {
            require(_recipients[i] != address(0));
            require(rewardToken.transfer(_recipients[i], _amount[i]));
        }

        emit AirDropStatus(_recipients, _amount);
        return true;
    }

    /**
    @dev Recover BEP20/ERC20 token from the contract address by owner
    @param _tokenAddress - cannot be the zero address.
    @param amount - cannot be greater than balance.
  */
    function withdrawBEP20(address _tokenAddress, uint256 amount)
        external
        onlyOwner
    {
        require(_tokenAddress != address(0), "Address cant be zero address");
        IERC20Upgradeable tokenContract = IERC20Upgradeable(_tokenAddress);
        require(
            amount <= tokenContract.balanceOf(address(this)),
            "Amount exceeds balance"
        );
        tokenContract.transfer(msg.sender, amount);
        emit TokenFromContractTransferred(_tokenAddress, msg.sender, amount);
    }

    /**
       @dev withdraw native currency from the contract address
     */
    function withdrawBNBFromContract() external onlyOwner whenNotPaused {
        uint256 nativeCrrency = address(this).balance;
        // require(amount <= address(this).balance);
        address payable _owner = payable(msg.sender);
        _owner.transfer(nativeCrrency);
        emit WithdrawBNBFromContract(msg.sender, nativeCrrency);
    }
}
